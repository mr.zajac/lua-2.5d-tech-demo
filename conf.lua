function love.conf (t)
  t.title = '3D Tech Demo'
  t.version = '0.9.1'
  t.window.width = 640
  t.window.height = 480

  t.console = true
end
