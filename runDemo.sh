#!/bin/bash

# Check if Lua Love is installed on system 
if [ -z `which love` ]; then
    echo "Love2d not installed on system. Visit https://love2d.org for more information"
fi

DEMO_DIR=`dirname $0`
LOVE_ARCHIVE="$DEMO_DIR"/Lua-2.5d-TechDemo.love

# check if Love archive was build and if so then run the demo 
if [ -a "$LOVE_ARCHIVE" ] ; then
    love "$LOVE_ARCHIVE"
else
    echo "Love archive not found, run 'make all'"
fi

