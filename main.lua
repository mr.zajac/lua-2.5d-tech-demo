--
-- Created by Michał Zając 
-- This file is distributed under MIT license
-- 
bump = require 'lib.bump.bump'

debug = true

local player = {
  x = 11.5, 
  y = 4.5, 
  angle = math.pi / 16,
  range = 15,
  speed = 2,
  viewType = 0,
  fov = math.pi / 3  -- 60 deg
}

world = nil

local mapGrid = {
  {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
  {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
  {'#', '#', '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#', '#', '#'},
  {'#', '#', '#', '.', '#', '#', '#', '#', '#', '.', '.', '.', '.', '#', '#', '#'},
  {'#', '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#', '.', '.', '#', '#'},
  {'#', '#', '.', '#', '#', '#', '.', '.', '#', '.', '.', '#', '.', '.', '#', '#'},
  {'#', '#', '.', '.', '.', '#', '.', '.', '#', '.', '.', '.', '.', '.', '#', '#'},
  {'#', '#', '.', '#', '.', '#', '.', '.', '#', '.', '.', '.', '.', '#', '#', '#'},
  {'#', '#', '.', '#', '#', '#', '#', '.', '#', '.', '.', '.', '.', '#', '#', '#'},
  {'#', '#', '.', '.', '.', '.', '.', '.', '#', '.', '.', '.', '.', '#', '#', '#'},
  {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
  {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
}

local walls = {}

function dist (p1, p2)
  return math.sqrt(math.pow(p1.x - p2.x, 2) + math.pow(p1.y - p2.y, 2))  
end

function getAngle (p1, p2)
  local dx = math.abs(p2.x - p1.x)
  local dy = math.abs(p2.y - p1.y)

  if p2.x >= p1.x and p2.y >= p1.y then
    return math.atan(dy / dx)
  elseif p2.x < p1.x and p2.y >= p1.y then
    return math.pi - math.atan(dy / dx)
  elseif p2.x < p1.x and p2.y < p1.y then
    return (3 * math.pi / 2) - math.atan(dx / dy)
  elseif p2.x >= p1.x and p2.y < p1.y then
    return (2 * math.pi) - math.atan(dy / dx)
  else
    return nil
  end
end

function line (p1, p2)
  return function (x)
    local a = (p2.y - p1.y) / (p2.x - p1.x)
    local b = (p1.y * p2.x - p1.x * p2.y) / (p2.x - p1.x)

    return a * x + b
  end
end

function invertLine (p1, p2)
  return function (y)
    local a = (p2.y - p1.y) / (p2.x - p1.x)
    local b = (p1.y * p2.x - p1.x * p2.y) / (p2.x - p1.x)

    return (y - b) / a
  end
end

function castRayStepForAngle (p1, angle)
  local x = p1.x + math.cos(angle)
  local y = p1.y + math.sin(angle)

  return castRayStepForVector(p1, {x = x, y = y})
end

function castRayStepForVector (p1, p2)
  local c_x = 0  -- nearest integer value on the X axis in the direction of p2
  local c_y = 0  -- nearest integer value on the Y axis in the direction of p2
  local l = line(p1, p2)
  local invL = invertLine(p1, p2)

  c_x = (p2.x - p1.x > 0 and math.ceil(p1.x)) or math.floor(p1.x)
  c_y = (p2.y - p1.y > 0 and math.ceil(p1.y)) or math.floor(p1.y)

  if dist(p1, {x = c_x, y = l(c_x)}) < dist(p1, {x = invL(c_y), y = c_y}) then
    return c_x, l(c_x)
  else
    return invL(c_y), c_y
  end
end

function castRayStep (p1, x)
  if type(x) == 'table' then
    return castRayStepForVector(p1, x)
  else
    return castRayStepForAngle(p1, x)
  end
end

function castRay (p1, rad)
  local point = {x = p1.x, y = p1.y}

  for i = 1, 5 do
    point.x, point.y = castRayStep(point, rad)
    print('Step: '.. point.x, point.y)
    point.x = point.x + 0.01 * math.cos(rad) -- quick fix, move the point just a litle so it won't land on a integer value
    point.y = point.y + 0.01 * math.sin(rad)
  end
end

function main ()
  local point = {}
  point.x = 1.5
  point.y = 1.5

  for rad = 0, 2 * math.pi, math.pi / 16 do
    print(math.deg(rad), castRayStep(point, rad))
    print(math.deg(rad), castRayStep(point, {x = point.x + 2*math.cos(rad), y = point.y + 2*math.sin(rad)} ))
    print('')
  end

  castRay(point, math.pi / 16)

end

-- Loading
function love.load(arg)

  world = bump.newWorld(1)

  -- create walls list from mapGrid
  for i, t in ipairs(mapGrid) do
    for j, v in ipairs(t) do
      if v == '#' then
        local w = {x = j - 1, y = #mapGrid - i, h = 1, w = 1}
        walls[#walls + 1] = w
        world:add(walls[#walls], w.x, w.y, w.h, w.w)
      end
    end
  end

  world:add(player, player.x - 0.25, player.y - 0.25, 0.5, 0.5)
end


-- Updating
function love.update(dt)
  if love.keyboard.isDown('escape') then
    love.event.push('quit')
  end

  if love.keyboard.isDown('left') then
    player.angle = player.angle + math.pi * dt
  end

  if love.keyboard.isDown('right') then
    player.angle = player.angle - math.pi * dt
  end

  if love.keyboard.isDown('up') then
    player.x = player.x + player.speed * dt * math.cos(player.angle)
    player.y = player.y + player.speed * dt * math.sin(player.angle)
  end

  if love.keyboard.isDown('down') then
    player.x = player.x - player.speed * dt * math.cos(player.angle)
    player.y = player.y - player.speed * dt * math.sin(player.angle)
  end

  player.x, player.y = world:move(player, player.x - 0.25, player.y - 0.25)

  player.x = player.x + 0.25
  player.y = player.y + 0.25

  if love.keyboard.isDown('q') then
    player.viewType = 0
  end

  if love.keyboard.isDown('w') then 
    player.viewType = 1
  end
end

-- Drawing
function love.draw(dt)
  local scale = 40

  if player.viewType == 0 then
    -- draw map
    love.graphics.setColor(0xff, 0xff, 0xff, 0xff)
    for _, w in ipairs(walls) do
      love.graphics.rectangle('fill', scale * w.x, 440 - scale * w.y, scale * w.w, scale * w.h)
    end

    -- draw grid
    love.graphics.setColor(0x00, 0x00, 0xff, 0xff)
    for i = 0, 640, scale do
      for j = 0, 480, scale do
        love.graphics.line(i, j, i, 480)
        love.graphics.line(i, j, 640, j)
      end
    end


    -- draw player
    love.graphics.setColor(0xff, 0xff, 0xff, 0xff)
    love.graphics.circle('fill', scale * player.x, 480 - scale * player.y, scale * 0.25)
    love.graphics.setColor(0xff, 0xff, 0x00, 0xff)

    local xTo = player.x + player.range * math.cos(player.angle)
    local yTo = player.y + player.range * math.sin(player.angle)
    love.graphics.line(scale * player.x, 480 - scale * player.y, scale * xTo, 480 - scale * yTo) 
  end

  if player.viewType == 0 then
    for rad = player.angle - player.fov / 2, player.angle + player.fov / 2, math.pi / 256 do
      local p = {x = player.x, y = player.y}
      local pPrev = {x = player.x, y = player.y} -- need for drawing after we break the loop
      love.graphics.setColor(0xff, 0x00, 0x00, 0xff)

      while true do 
        p.x, p.y = castRayStep(p, rad)
        if dist(player, p) < player.range then
          -- love.graphics.circle('fill', scale * p.x, 480 - scale * p.y, scale * 0.1)
          -- print('Step: '.. p.x, p.y)
          pPrev.x = p.x
          pPrev.y = p.y
          p.x = p.x + 0.01 * math.cos(rad) -- quick fix, push the p point so it won't have the same value we started with
          p.y = p.y + 0.01 * math.sin(rad)

          local c_x = math.floor(p.x + 1)
          local c_y = math.floor(p.y + 1) - 1
        
          if mapGrid[#mapGrid - c_y][c_x] == '#' then
            -- we hit the wall with our ray
            break
          end
        else
          break
        end
      end
    
      love.graphics.setColor(0xff, 0x00, 0x00, 0x7f)
      love.graphics.line(scale * player.x, 480 - scale * player.y, scale * pPrev.x, 480 - scale * pPrev.y)
    end
  else
    local rad = player.angle - player.fov / 2 

    for i = 640, 0, -1 do
      local p = {x = player.x, y = player.y}
      local pPrev = {x = player.x, y = player.y}
      local wallHit = false

      while true do
        p.x, p.y = castRayStep(p, rad)
        if dist(player, p) < player.range then
          pPrev.x = p.x
          pPrev.y = p.y
          p.x = p.x + 0.01 * math.cos(rad) -- quick fix, push the p point so it won't have the same value we started with
          p.y = p.y + 0.01 * math.sin(rad)

          local c_x = math.floor(p.x + 1)
          local c_y = math.floor(p.y + 1) - 1
        
          if mapGrid[#mapGrid - c_y][c_x] == '#' then
            -- we hit the wall with our ray
            wallHit = true
            break
          end
        else
          wallHit = false
          break
        end
      end -- end while
        -- draw 3D view
        if wallHit then
          local dist2Wall = dist(player, pPrev) * math.abs(math.cos(rad - player.angle))
          local wallH = 0.5 / dist2Wall
          
          love.graphics.setColor(math.min(0xcc / dist2Wall, 0xcc), math.min(0xcc / dist2Wall, 0xcc), math.min(0xcc / dist2Wall, 0xcc), 0xff)
          love.graphics.line(i, math.min((480 - 1000 * wallH) / 2, 240), i, math.max(480 - ((480 - 1000 * wallH)/2) , 240))
        end

        rad = rad + player.fov / 640
    end
  end

  if debug then
    local fps = tostring(love.timer.getFPS())

    love.graphics.setColor(0xff, 0x00, 0x00, 0xff)
    love.graphics.print('FPS: ' .. fps, 9, 10)
  end
end
