SOURCE=conf.lua \
	   main.lua \
	   LICENSE.txt \
	   README.md \

LIB=lib/bump
	   
all: Lua-2.5d-TechDemo.love 

Lua-2.5d-TechDemo.love: $(SOURCE) $(LIB)
	zip -9 -r Lua-2.5d-TechDemo.love $(LIB) $(SOURCE)

lib/bump:
	unzip lib/bump.zip -d lib

.PHONY: clean

clean:
	rm -f *.love
	rm -rf lib/bump
